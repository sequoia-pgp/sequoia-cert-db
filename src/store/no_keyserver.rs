//! Dummy replacement for the keyserver store.

use std::sync::Arc;
use sequoia_openpgp::{Fingerprint, KeyHandle};
use crate::{
    LazyCert,
    Result,
    Store,
    store::UserIDQueryParams,
};

/// Replacement for the keyserver store if feature `keyserver` is
/// disabled.
///
/// This type cannot be constructed.  Hence, its methods cannot be
/// invoked.
pub struct KeyServer<'a> {
    spooky: std::marker::PhantomData<&'a ()>,
}

impl<'a> KeyServer<'a> {
    /// Deletes the key with the given fingerprint from the cache.
    #[allow(dead_code)]
    pub(crate) fn delete_from_cache<I>(&self, _: I)
        where I: Iterator<Item=Arc<LazyCert<'a>>>,
    {
        unreachable!("no instance can be created")
    }
}

impl<'a> Store<'a> for KeyServer<'a> {
    fn lookup_by_cert(&self, _: &KeyHandle) -> Result<Vec<Arc<LazyCert<'a>>>> {
        unreachable!("no instance can be created")
    }

    fn lookup_by_cert_or_subkey(&self, _: &KeyHandle) -> Result<Vec<Arc<LazyCert<'a>>>> {
        unreachable!("no instance can be created")
    }

    fn select_userid(&self, _: &UserIDQueryParams, _: &str)
        -> Result<Vec<Arc<LazyCert<'a>>>>
    {
        unreachable!("no instance can be created")
    }

    fn fingerprints<'b>(&'b self) -> Box<dyn Iterator<Item=Fingerprint> + 'b> {
        unreachable!("no instance can be created")
    }
}
